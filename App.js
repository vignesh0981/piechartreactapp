/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Dimensions,TouchableHighlight, View} from 'react-native';
import Pie from 'react-native-pie';
import { PieChart } from 'react-native-svg-charts'
import axios from 'axios';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
import {SkypeIndicator} from 'react-native-indicators';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedSlice: {
        label: '',
        value: 0
      },
      labelWidth: 0,
      PieChartData:[],
      keys:[],
      values:[],
      colors:[],
      Apiloaded:false
    }
  }
  componentDidMount(){

   this.getAllitems();
  
  }

  getAllitems(){
    axios.get('https://stark-oasis-22977.herokuapp.com/api/piechart/getallpiechart')
    .then(res =>{
       // console.log(res)
        var keys =[],values=[],colors=[];
       for(var i=0;i<res.data.length;i++){
         console.log(res.data[i])
         keys.push(res.data[i].name);
         values.push(parseInt(res.data[i].value))
         colors.push(res.data[i].color);
       }
       this.setState({keys,values,colors,Apiloaded:true}); //set piecharts values
       this.setState({selectedSlice:{    //Set first data as default data
         label:keys[0],
         value:values[0]
       }})
    }).catch( err =>{
    console.warn(err)
    })
  }
  
  render() {
    if(!this.state.Apiloaded){  // loader until api is loaded
      return (       
         <View style={{ justifyContent: 'center', flex: 1 }}>
            <SkypeIndicator color='black' />
        
        </View>)
      
    }
    const { labelWidth, selectedSlice } = this.state;
    var { label, value } = selectedSlice;
    if(value === 0){
      value = '';
    }
    const {keys,values,colors} = this.state;
    const data = keys.map((key, index) => {
        return {
          key,
          value: values[index],
          svg: { fill: colors[index] },
          arc: { outerRadius: (70 + values[index]) + '%', padAngle: label === key ? 0.1 : 0 },
          onPress: () => this.setState({ selectedSlice: { label: key, value: values[index] } })
        }
      })
    const deviceWidth = Dimensions.get('window').width

    return (
      <View style={{ justifyContent: 'center', flex: 1 }}>
        <PieChart
          style={{ height: 200 }}
          outerRadius={'40%'}
          innerRadius={'100%'}
          data={data}
        />
        <Text
          onLayout={({ nativeEvent: { layout: { width } } }) => {
            this.setState({ labelWidth: width });
          }}
          style={{
            position: 'absolute',
            left: deviceWidth / 2 - labelWidth / 2,
            textAlign: 'center'
          }}>
          {`${label} \n ${value} days`}
        </Text>

        <View style={{ justifyContent: 'center', alignItems: 'center',bottom:30,position:'absolute',left:(Dimensions.get('window').width/2)/2}}>
        <TouchableHighlight
         style={styles.button}
         onPress={this.onPress}
        >
         <Text> Refresh </Text>
        </TouchableHighlight>
        </View>
      </View>
    )
  }

  onPress = () => {
    this.setState({Apiloaded:false})
   this.getAllitems();
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
   justifyContent: 'space-around',
    marginTop:30,
  },
  gauge: {
    position: 'absolute',
    width: 140,
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
  },
  gaugeText: {
    backgroundColor: 'transparent',
    color: '#000',
    fontSize: 24,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    marginTop:15,
    width:Dimensions.get('window').width/2
  },
});
